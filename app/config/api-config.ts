/* App config for apis
 */
const ApiConfig = {
  BASE_URL: 'https://api.covid19api.com/', // 'http://wolverine-dev.com/',
  LOGIN: 'api/login',
};

export default ApiConfig;
