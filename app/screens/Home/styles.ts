import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    // backgroundColor: 'pink',
    // marginHorizontal: 5,
  },
  card: {
    margin: 10,
    paddingBottom: 10,
    borderRadius: 15,
  },
  list: {
    margin: -8,
    marginLeft: 10,
  },
  title: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  menuview: {
    paddingTop: 50,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  content: { flexDirection: 'row', justifyContent: 'space-between' },
});

export default styles;
