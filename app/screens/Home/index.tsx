/* eslint-disable quotes */
/* eslint-disable comma-dangle */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { View, FlatList, RefreshControl, ScrollView, SafeAreaView, LogBox, Dimensions } from 'react-native';
import { Appbar, Avatar, Button, Card, Divider, List, Menu, Paragraph, Provider, Text, Title } from 'react-native-paper';
import { useStore } from 'app/store';

import styles from './styles';
import { GetSurevayDetails, GetUserDetails } from 'app/services/react-query/queries/user';
import NavigationService from 'app/navigation/NavigationService';
import { useEffect } from 'react';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import { PureChart } from 'react-native-pure-chart';
import { Country, Global } from 'app/models/api/summary';

const Home: React.FC = () => {
  const setIsLoggedIn = useStore(state => state.setIsLoggedIn);
  // const { isLoading, isFetching, data = { results: [] } } = GetUserDetails();
  const { isLoading, isFetching, data = { Global: {}, Countries: [] } } = GetSurevayDetails();
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  const [globalData, setGlobalData] = useState(data.Global);

  const onLogOut = () => {
    setIsLoggedIn(false);
  };

  const onSeeMore = () => NavigationService.navigate('More');

  const CustomLineChart = props => (
    <View>
      <Text>Bezier Line Chart</Text>
      <LineChart
        data={{
          labels: props.data.item.map(({ Date }) => Date),
          datasets: [{
            data: props.data.item.map(({ TotalConfirmed }) => TotalConfirmed),
            color: (opacity = 1) => `rgba(105, 137, 52, ${opacity})`, // optional
          }, {
            data: props.data.item.map(({ TotalDeaths }) => TotalDeaths),
            color: (opacity = 1) => `rgba(219, 23, 23, ${opacity})`, // optional
          }],
          legend: ['Infected', 'Deaths'] // optional
        }}
        width={Dimensions.get('window').width} // from react-native
        height={350}
        yAxisLabel="$"
        yAxisSuffix="k"
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#000000',
          backgroundGradientTo: '#ffffff',
          decimalPlaces: 1, // optional, defaults to 2dp
          color: (opacity = 1) => 'rgba(255, 255, 255, 0.5)',
          labelColor: (opacity = 1) => 'rgba(255, 255, 255, 0.5)',
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: '3',
            strokeWidth: '1',
            // stroke: '#ffa726',
          },
        }}
        bezier
        style={{
          margin: 10,
          borderRadius: 10,
        }}
      />
    </View>
  );

  const CustomBarChart = props => (
    <BarChart
      width={Dimensions.get('window').width - 52}
      height={256}
      data={
        {
          labels: ["Infected", "Deaths", "Recovered"],
          datasets: [
            {
              data: [props.cont.item.TotalConfirmed, props.cont.item.TotalDeaths, props.cont.item.TotalRecovered]
            }
          ]
        }
      }
      withHorizontalLabels={false}
      showValuesOnTopOfBars={true}
      // yAxisLabel="k"
      chartConfig={{
        backgroundColor: '#022173',
        backgroundGradientFrom: "#022173",
        backgroundGradientTo: "#1b3fa0",
        color: (opacity = 1) => `rgba(${255}, ${255}, ${255}, ${opacity})`,
        style: {
          borderRadius: 16,
        },
      }}
      style={{
        borderRadius: 15,
        marginTop: 15,
      }}
    />
  );

  const renderListItem = ({ item }) => (
    <List.Item
      style={styles.list}
      title={item.Country + ' | ' + item.CountryCode}
      description={`Confirmed: ${item.TotalConfirmed}\nRecovered: ${item.TotalRecovered}\nDeaths: ${item.TotalDeaths}`}
      descriptionNumberOfLines={0}
      // left={props => <Avatar.Icon {...props} icon="web" />} //<List.Icon {...props} icon="web" />}
      left={LeftContent}
    />
  );

  const LeftContent = props => <Avatar.Icon {...props} icon="web" />;
  const MyComponent = props => (
    <Card style={styles.card} mode="elevated">
      <Card.Title title="Global Data" subtitle="Covid-19 Status" left={LeftContent} />
      <Card.Content>
        <Paragraph>Total Confirmed: {props.data.item.TotalConfirmed}</Paragraph>
        <Paragraph>Total Recovered: {props.data.item.TotalRecovered}</Paragraph>
        <Paragraph>Total Deaths: {props.data.item.TotalDeaths}</Paragraph>
        <Divider />
        {data.Global ?
          <CustomBarChart cont={{ item: data.Global }} />
          : <></>}

      </Card.Content>
    </Card>
  );

  return (
    <View style={styles.container}>
      <Appbar.Header>
        <Appbar.Content title="COVID-19 Survay" subtitle="Dashboard" />
        <Appbar.Action icon="logout" onPress={onLogOut} />
      </Appbar.Header>

      <ScrollView style={styles.scrollView} keyboardDismissMode="none">
        <Card style={styles.card} mode="elevated">
          <Card.Title style={{ alignContent: 'center' }} title="Top Countries" />

          <FlatList
            scrollEnabled={false}
            data={data.Countries.sort(function (a, b) {
              return b.TotalConfirmed - a.TotalConfirmed;
            }).slice(0, 5).map((item) => {
              return item;
            })}
            renderItem={renderListItem}//{renderItem}
            keyExtractor={item => item.ID}
            refreshControl={
              <RefreshControl
                refreshing={isLoading || isFetching}
                onRefresh={() => { }}
              />
            }
          />
          <Card.Actions>
            <Button onPress={onSeeMore} color="#FEB557">See more</Button>
            {/* <Button>Ok</Button> */}
          </Card.Actions>
        </Card>

        {!isFetching && globalData ?
          <MyComponent data={{ item: globalData }} /> : <></>
        }

        {/* <CustomLineChart data={{
          item: data.Countries.sort(function (a, b) {
            return b.Date - a.Date;
          })
        }} /> */}
        {/* {data.Global ?
          <CustomBarChart cont={{ item: data.Global }} />
          : <></>} */}
      </ScrollView>
    </View>

  );
};

export default Home;
