/* eslint-disable no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable jsx-quotes */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { View, FlatList, RefreshControl, EventEmitter, Image } from 'react-native';
import { Appbar, Avatar, Card, Divider, List, Menu, Paragraph, Searchbar } from 'react-native-paper';
import { useStore } from 'app/store';

import styles from './styles';
import { GetSurevayDetails } from 'app/services/react-query/queries/user';
import NavigationService from 'app/navigation/NavigationService';
import { Country } from 'app/models/api/summary';
import filter from 'lodash.filter';


const More: React.FC = () => {
  const setIsLoggedIn = useStore(state => state.setIsLoggedIn);
  const { isLoading, isFetching, data = { Global: {}, Countries: [] } } = GetSurevayDetails();
  // const isDark = useStore(state => state.isDarkMode);
  console.disableYellowBox = true;

  // let eventEmitter = new EventEmitter();
  // const listener1 = (e) => {
  //   console.dir(`listener1`);
  // };
  //Menu Component
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  const onLogOut = () => {
    setIsLoggedIn(false);
  };
  const onBack = () => NavigationService.navigate('Home');
  const [tempData, setTempData] = useState<Country[]>(data.Countries);

  const sortBy = (type) => {
    // setFullData(tempData);
    if (type === 'deaths') {
      setFullData(fullData.sort(function (a, b) {
        return b.TotalDeaths - a.TotalDeaths;
      }));
    } else if (type === 'active') {
      setFullData(fullData.sort(function (a, b) {
        return b.TotalConfirmed - a.TotalConfirmed;
      }));
    } else {
      setFullData(fullData.sort(function (a, b) {
        return b.TotalConfirmed - a.TotalConfirmed;
      }));
    }
    // eventEmitter.removeAllListeners();
    setVisible(false);
  };



  const [fullData, setFullData] = useState<Country[]>(data.Countries);

  const handleSearch = text => {
    const formattedQuery = text.toLowerCase();
    const filteredData = filter(tempData, item => {
      return contains(item, formattedQuery);
    });
    setFullData(filteredData);
  };
  // eslint-disable-next-line no-shadow
  const contains = ({ Country, CountryCode }, query) => {
    console.log(Country, CountryCode);
    if (Country.toLowerCase().includes(query) || CountryCode.toLowerCase().includes(query)) {
      return true;
    }
    return false;
  };

  const [searchQuery, setSearchQuery] = React.useState('');
  useEffect(() => {
    console.log('useeffect', searchQuery);
    setFullData(tempData);
    handleSearch(searchQuery);
  }, [searchQuery]);

  const onChangeSearch = query => {
    console.log('onChangeSearch');
    setSearchQuery(query);
  };
  const onCloseIconPress = () => setFullData(tempData);

  const LeftContent = props => <Avatar.Icon {...props} style={styles.avatar} icon="web" />;
  const renderListItem = ({ item }) => (
    <View>
      <List.Item
        style={styles.list}
        title={item.Country + " | " + item.CountryCode}
        description={`Total Confirmed: ${item.TotalConfirmed}\nTotal Recovered: ${item.TotalRecovered}\nTotal Deaths: ${item.TotalDeaths}\nNew Confirmed: ${item.NewConfirmed}\nNew Recovered: ${item.NewRecovered}\nNew Deaths: ${item.NewDeaths}`}
        descriptionNumberOfLines={0}
        left={LeftContent}//{props => <List.Icon {...props} icon="web" />}
      />
      <Divider style={{ marginHorizontal: 15 }} />
    </View>
  );

  return (
    <View style={styles.container}>
      <Appbar.Header>
        <Appbar.Action icon='chevron-left' onPress={onBack} />
        <Appbar.Content title="COVID-19 Survay" />
        <Menu
          onDismiss={closeMenu}
          visible={visible}
          anchor={
            <Appbar.Action
              // disabled={isLoading}
              // color="white"
              icon={'sort'}
              onPress={openMenu}
            />
          }>
          <Menu.Item icon="sort-variant" title="Sort By Deaths" onPress={() => { sortBy('deaths'); }} key='deaths' />
          {/* <Divider /> */}
          <Menu.Item icon="sort-variant" title="Sort By Active Cases" onPress={() => { sortBy('active'); }} key='active' />
          {/* <Divider /> */}
          <Menu.Item icon="sort-variant" title="Sort By Recovery" onPress={() => { sortBy('recovery'); }} key='recovery' />
          {/* <Menu.Item onPress={() => { }} title="Item 3" /> */}
        </Menu>
      </Appbar.Header>


      <Card style={styles.card} mode="elevated">
        <Card.Title style={{ alignContent: 'center' }} title="Top Countries" />
        <Searchbar
          style={styles.search}
          placeholder="Search"
          onChangeText={onChangeSearch}
          value={searchQuery}
          onIconPress={onCloseIconPress}
        // clearButtonMode='always'
        />
        <FlatList
          keyboardDismissMode="none"
          // ListHeaderComponent={}//{renderHeader}
          data={fullData}
          renderItem={renderListItem}//{renderItem}
          keyExtractor={item => item.Slug}
          refreshControl={
            <RefreshControl
              refreshing={isLoading || isFetching}
              onRefresh={() => { }}
            />
          }
        />
      </Card>
    </View>
  );
};

export default More;
