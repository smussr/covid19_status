import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 50,
  },
  card: {
    margin: 10,
    paddingBottom: 10,
    borderRadius: 15,
  },
  search: {
    marginHorizontal: 10,
    borderRadius: 25,
    marginBottom: 10,
  },
  list: {
    margin: -8,
    marginLeft: 10,
  },
  title: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  avatar: {
    width: 50,
    height: 50,
    marginTop: 30,
  },
  content: { flexDirection: 'row', justifyContent: 'space-between' },
});

export default styles;
