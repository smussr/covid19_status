/* eslint-disable prettier/prettier */
import React from 'react';
import { ImageBackground, TouchableOpacity, View } from 'react-native';
import { Text, Button, Card } from 'react-native-paper';

import styles from './styles';
import NavigationService from 'app/navigation/NavigationService';
import { useStore } from 'app/store';
import { Image } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';

const Login: React.FC = () => {
  const setIsLoggedIn = useStore(state => state.setIsLoggedIn);
  const isDark = useStore(state => state.isDarkMode);

  const onLogin = () => {
    setIsLoggedIn(true);
  };
  const onForgot = () => NavigationService.navigate('ForgotPassword');
  return (
    // <View style={styles.container}>
    <View style={styles.containerMain}>
      <ImageBackground source={isDark ? require('../../assets/lgn_bck.png') : require('../../assets/lgn_bck_nrml.png')}
        style={{ width: '100%', height: '100%' }}>
        <View style={styles.container}>
          {/* <Text>Inside</Text>
          <Text style={styles.login}>Login Status </Text>
           <Button icon="login" mode="outlined" onPress={onLogin}>
            Login
          </Button>
          {/* <Button
            mode="text"
            style={styles.forgot}
            labelStyle={styles.labelStyle}
            onPress={onForgot}>
            Forgot Password
          </Button> */}
        </View>
      </ImageBackground>
      <Card style={styles.bottomView} mode='outlined' >
        {/* <Card.Title style={{ alignContent: 'center' }} title="" /> */}
        <Card.Content>
          <Text style={styles.login}>Wear a Mask</Text>
          <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 10 }}>  Save lives</Text>
        </Card.Content>
        <Card.Actions>
          <TouchableOpacity
            onPress={onLogin}
            style={isDark ? styles.roundButton1 : styles.roundButton}>
            <Text style={isDark ? styles.labelStyleDark : styles.labelStyle}>CHECKOUT</Text>
          </TouchableOpacity>

        </Card.Actions>
      </Card>
      {/* </ImageBackground> */}
    </View>
    // </View>
  );
};

export default Login;
