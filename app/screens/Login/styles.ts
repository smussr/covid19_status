import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
  },
  card: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    // marginBottom: 10,
    color: 'red',
    backgroundColor: 'red',
  },
  login: {
    fontSize: 18,
  },
  forgot: {
    marginTop: 12,
  },
  labelStyle: {
    fontSize: 16,
    color: 'white',
  },
  labelStyleDark: {
    fontSize: 16,
    color: '#1b3fa0',
  },

  containerMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bottomView: {
    width: '100%',
    height: 230,
    // backgroundColor: '#EE5407',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute', //Here is the trick
    bottom: 0, //Here is the trick
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
  },
  roundButton: {
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: '#1b3fa0',
    color: '#1b3fa0',
    borderColor: '#122A6D',
    borderWidth: 2,

    shadowColor: 'blue',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 1,
    shadowRadius: 10.78,
    elevation: 12,
  },
  roundButton1: {
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: '#EE5407',
    color: '#EE5407',
    borderColor: '#BA4003',
    borderWidth: 2,

    shadowColor: 'red',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 1,
    shadowRadius: 10.78,
    elevation: 12,
  },
  textlogin: {

  }
});

export default styles;
