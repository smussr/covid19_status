/* eslint-disable prettier/prettier */
export interface Summary {
  id:        string;
  message:   string;
  global:    Global;
  countries: Country[];
}

export interface Country {
  ID:             string;
  Country:        string;
  CountryCode:    string;
  Slug:           string;
  NewConfirmed:   number;
  TotalConfirmed: number;
  NewDeaths:      number;
  TotalDeaths:    number;
  NewRecovered:   number;
  TotalRecovered: number;
  Date:           string;
  premium:        Premium;
}

export interface Premium {
}

export interface Global {
  NewConfirmed:   number;
  TotalConfirmed: number;
  NewDeaths:      number;
  TotalDeaths:    number;
  NewRecovered:   number;
  TotalRecovered: number;
  Date:           Date;
}
