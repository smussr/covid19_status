import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'https://api.covid19api.com/', //'https://rickandmortyapi.com/api',
  // baseURL: 'https://rickandmortyapi.com/api',
  responseType: 'json',
  withCredentials: true,
});

export { apiClient };
